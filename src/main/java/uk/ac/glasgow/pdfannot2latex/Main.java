package uk.ac.glasgow.pdfannot2latex;

import static java.lang.String.format;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import uk.ac.glasgow.pdfannot2latex.CommentedPdfDocument.SourceFormat;


public class Main {
	
	public static void main (String[] args){
		System.out.println("Starting pdf annotation extraction utility.");
			
		Options options = new Options();
			
		Option patchableSource = OptionBuilder
			.hasArg()
			.withDescription("Path of the original latex source file to patch.")
			.withLongOpt("patchable-src")
			.create('p');
			
		Option listFile = OptionBuilder
			.hasArg()
			.withDescription("Output file path for extracted and list formatted annotations.")
			.withLongOpt("list-file")
			.create('l');
			
		Option commentTemplate = OptionBuilder
			.hasArg()
			.withDescription("Template for comment list file.  Defaults to comment-template.  the tag COMMENTS will be replaced with a list of comments in the output file.")
			.withLongOpt("comment-template")
			.create('c');
		
		Option format = OptionBuilder
			.withDescription("Output comment format.")
			.withLongOpt("format")
			.hasArgs()
			.create();
						
		options
		.addOption(patchableSource)
		.addOption(listFile)
		.addOption(commentTemplate)
		.addOption(format);
			
		try {
			
			CommandLineParser parser = new BasicParser();
			try {
				CommandLine cmd = parser.parse( options, args);
				
				String pdfFileName = cmd.getArgs()[0];
				CommentedPdfDocument doc = 
					new CommentedPdfDocument(pdfFileName);
				
				System.out.println(format("Extracting comments from file [%s].", pdfFileName));
				
				SourceFormat sourceFormat = SourceFormat.LaTeX;
				if (cmd.hasOption("format"))
				    sourceFormat = cmd.getOptionValue("format").equals("markdown") ? SourceFormat.Markdown : SourceFormat.LaTeX;
				
				if (cmd.hasOption('l')) {
					String templateFilePath =
						cmd.getOptionValue('c', "comment-template.tex");
					String  commentOutputPath = cmd.getOptionValue('l');
					generateCommentListViewDocument(doc, templateFilePath, commentOutputPath, sourceFormat);

				}
				if (cmd.hasOption('p')){
				    String sourceFilePath = cmd.getOptionValue('p');
				    generatePatchedSourceDocumentFile(sourceFilePath, doc, sourceFormat);
				}
			
			} catch (ParseException e) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp( "pdfannot2latex", options );
			}
		} catch (IOException ioe){
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("pdfannot2latex", options);
		}

	}
	
	private static void generateCommentListViewDocument(
		CommentedPdfDocument doc, String templateFilePath, String outputLaTeXFilePath,
		SourceFormat sourceFormat){
		try {
			String commentDocTemplate = readTextFile(templateFilePath);
			String body = doc.generateListOfComments(sourceFormat);
			
			String result = commentDocTemplate.replace("COMMENTS", body);
			
			writeTextToFile(result, outputLaTeXFilePath);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static void generatePatchedSourceDocumentFile(String sourceFilename, CommentedPdfDocument doc, SourceFormat sourceFormat) {
		try {
			String source = readTextFile(sourceFilename);
			String patchedDocument = doc.generatePatchedSourceDocument(source, sourceFormat);
			String sourceFilenamePrefix = sourceFilename.substring(0, sourceFilename.lastIndexOf('.'));
			String sourceFilenameSuffix = sourceFilename.substring(sourceFilename.lastIndexOf('.'), sourceFilename.length());
			
			writeTextToFile(patchedDocument, sourceFilenamePrefix + "-patched" + sourceFilenameSuffix);
			
		} catch (IOException e) {
			// TODO log a warning
			e.printStackTrace();
		}
	}
	
	private static String readTextFile(String filename) throws IOException{

		FileInputStream fis = null;
		try {
			fis = new FileInputStream (filename);
		
			byte[] bytes = new byte[fis.available()];
			
			fis.read(bytes);
			
			return new String(bytes);
		}
		finally {
			if (fis != null)
				fis.close();
		}
		
	}
	
	private static void writeTextToFile(String source, String filename) {
		try {
			FileOutputStream fos = new FileOutputStream(filename);
			fos.write(source.getBytes());
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
}