package uk.ac.glasgow.pdfannot2latex;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.itextpdf.awt.geom.Rectangle;
import com.itextpdf.text.pdf.PdfArray;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfString;
import com.itextpdf.text.pdf.parser.FilteredTextRenderListener;
import com.itextpdf.text.pdf.parser.LocationTextExtractionStrategy;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import com.itextpdf.text.pdf.parser.RegionTextRenderFilter;
import com.itextpdf.text.pdf.parser.RenderFilter;
import com.itextpdf.text.pdf.parser.TextExtractionStrategy;

import uk.ac.glasgow.pdfannot2latex.comments.Comment;
import uk.ac.glasgow.pdfannot2latex.comments.Highlight;
import uk.ac.glasgow.pdfannot2latex.comments.Replace;
import uk.ac.glasgow.pdfannot2latex.comments.Sticky;

public class CommentedPdfPage {
	
	private int pageNumber;
	
	private Set<PdfDictionary> processedAnnotations;
	
	private PdfReader reader;
	
	private List<Comment> comments;

	public CommentedPdfPage(int pageNumber, PdfReader reader, PdfArray annotations) {
		this.pageNumber = pageNumber;
		this.reader = reader;
		this.comments = new ArrayList<Comment>();
		this.processedAnnotations = new HashSet<PdfDictionary>();

		for (PdfObject obj : annotations) {

			PdfDictionary annot = (PdfDictionary) PdfReader.getPdfObject(obj);
			if (processedAnnotations.contains(annot)) continue;

			PdfObject subtype = annot.get(PdfName.SUBTYPE);

			if (subtype == PdfName.STRIKEOUT) 
				handleStrikeout(annot);
			else if (subtype.toString().equals("/Caret"))
				handleInsert(annot);
			else if (subtype == PdfName.HIGHLIGHT)
				handleHighlight(annot);
			else if (subtype == PdfName.TEXT)
				handleText(annot);
			else if (subtype == PdfName.POPUP)
				continue;
			else 
				System.err.println("Unknown annotation type ["+subtype+"].");
				
			processedAnnotations.add(annot);
		}
	}
	
	private void handleText(PdfDictionary annotation) {  	
		
		String comment = annotation.getAsString(PdfName.CONTENTS).toString();
		
		comments.add(new Sticky(comment));
	}

	private void handleHighlight(PdfDictionary annotation) {

		PdfString content = annotation.getAsString(PdfName.CONTENTS);
		if (content == null)
			return;

		String comment = annotation.getAsString(PdfName.CONTENTS).toString();
		
		Rectangle rect = extractCommentedChunkRectangle(annotation);
		
		String highlightedText = null;
		try {
			highlightedText = extractText(rect);
		} catch(IOException ioe) {
			//TODO
		}
		
		comments.add(new Highlight(highlightedText,comment));
	}

	private void handleInsert(PdfDictionary annot) {
	
		String textToInsert = annot.getAsString(PdfName.CONTENTS).toString();
		
		handleReplaceText("",textToInsert,annot);			
	}

	private void handleStrikeout(PdfDictionary annotation) {
		
		Rectangle rect = extractCommentedChunkRectangle(annotation);
		rect.width -= 6;
		rect.x += 3;
					
		String textToRemove = null;
		try {
			textToRemove = extractText(rect);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PdfDictionary insertAnnot = annotation.getAsDict(PdfName.IRT);
		String textToInsert = "";
		if (insertAnnot != null){
			
			processedAnnotations.add(insertAnnot);
			PdfString textToInsertPdfString = insertAnnot.getAsString(PdfName.CONTENTS);
			if (textToInsertPdfString != null)
				textToInsert = textToInsertPdfString.toString();
		}
			
						
		handleReplaceText(textToRemove,textToInsert,annotation);
	}
	
	private void handleReplaceText(
				String textToRemove, String textToInsert, PdfDictionary annotation){
		
		Rectangle rect = extractCommentedChunkRectangle(annotation);
		
		String preText = null;
		try {
			preText = extractPreText(rect,50.0f);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String postText = null;
		try {
			postText = extractPostText(rect, 50.0f);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
					
		//rect.width -= 40;
		//rect.x -= 6;
					
		comments.add(new Replace(preText,textToRemove,textToInsert,postText)); 			
	}
	
	private Rectangle extractCommentedChunkRectangle(PdfDictionary annot) {
		
		PdfArray rectPdfArray = annot.getAsArray(PdfName.RECT);
		
		float x = rectPdfArray.getAsNumber(0).floatValue();		 
		float y = rectPdfArray.getAsNumber(1).floatValue();
		 
		float width = rectPdfArray.getAsNumber(2).floatValue()-x;
		float height = rectPdfArray.getAsNumber(3).floatValue()-y;
		 		 
		return new Rectangle(x,y,width,height);	
	}
	
	private String extractPreText(Rectangle rect, float length) throws IOException{
		Rectangle preRect = new Rectangle(rect.x-length,rect.y,length-5.0,rect.height);
		return extractText(preRect);
	}
	
	private String extractPostText(Rectangle rect, float length) throws IOException{
		Rectangle postRect = new Rectangle(rect.x+rect.width,rect.y,length-5.0,rect.height);
		return extractText(postRect);
	}

	private String extractText(Rectangle rect) throws IOException {
		RenderFilter filter = new RegionTextRenderFilter(rect);
		 
		TextExtractionStrategy location =
				new LocationTextExtractionStrategy();
			 
		 TextExtractionStrategy strategy =
				 new FilteredTextRenderListener(location, filter);
			 
		return PdfTextExtractor.getTextFromPage(reader, pageNumber, strategy);
	}
	
	public List<Comment> getComments(){
		List<Comment> results = new ArrayList<Comment>();
		results.addAll(comments);
		return results;
	}
	
	public List<Replace> getReplaces(){
		List<Replace> results = new ArrayList<Replace>();
		for (Comment comment: comments)
			if (comment instanceof Replace)
				results.add((Replace)comment);
		return results;	
	}
	
	public String getLaTeXListViewOfComments() {
		String result = "";

		for (Comment comment: comments)
			result+="\\item[pp"+pageNumber+"] "+comment.getLaTeXText()+"\n\n";
		
		return result;
	}

	public String getMarkdownListViewOfComments(){
		String result = "";

		for (Comment comment: comments)
			result += "* (pp" + pageNumber + ")" + comment.getMarkdownText() + "\n\n";

		return result;
	}
}
