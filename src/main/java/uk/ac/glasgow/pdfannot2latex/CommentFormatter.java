package uk.ac.glasgow.pdfannot2latex;

import uk.ac.glasgow.pdfannot2latex.comments.DocumentPatch;
import uk.ac.glasgow.pdfannot2latex.comments.Highlight;
import uk.ac.glasgow.pdfannot2latex.comments.Sticky;

public interface CommentFormatter {
	
	public String formatAsListItem(Highlight highlight);
	
	public String formatAsListItem(DocumentPatch documentPatch);
	
	public String formatAsListItem(Sticky sticky);

}
