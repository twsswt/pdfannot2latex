package uk.ac.glasgow.pdfannot2latex;

import static java.lang.String.format;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import com.itextpdf.text.pdf.PdfArray;
import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfReader;

import uk.ac.glasgow.pdfannot2latex.comments.Comment;
import uk.ac.glasgow.pdfannot2latex.comments.Replace;

import javax.xml.transform.Source;

public class CommentedPdfDocument {
    	
    	public enum SourceFormat {
    	    LaTeX, Markdown
    	}

	private PdfReader reader;

	private final SortedMap<Integer,CommentedPdfPage> commentedPages =
			new TreeMap<Integer,CommentedPdfPage>();
	
	public CommentedPdfDocument (String filename) throws IOException {
		reader = new PdfReader(filename);
			   
		for (int pageNumber = 1; pageNumber <= reader.getNumberOfPages(); pageNumber++) {				 

			PdfDictionary page = reader.getPageN(pageNumber);
			PdfArray annotations = page.getAsArray(PdfName.ANNOTS);

			if (annotations != null) 
				commentedPages.put(pageNumber, new CommentedPdfPage(pageNumber,reader, annotations));
		}
	}

	public String generateListOfComments(SourceFormat sourceFormat) {
		if (sourceFormat == SourceFormat.Markdown)
			return generateMarkdownListOfComments();
		else if (sourceFormat == SourceFormat.LaTeX)
			return generateLaTeXListOfComments();
		else
			return "";
	}
		
	private String generateLaTeXListOfComments () {
		String header = "\\begin{list}{}{}\n\n";
		String body = "";
		String closing = "\\end{list}";
		
		for (Integer pageNumber: commentedPages.keySet()){
			CommentedPdfPage commentedPage = commentedPages.get(pageNumber); 
			body += commentedPage.getLaTeXListViewOfComments();
		}
		
		return header + body + closing;
	}

	private String generateMarkdownListOfComments(){
		String body = "";
		for (Integer pageNumber: commentedPages.keySet()){
			CommentedPdfPage commentedPage = commentedPages.get(pageNumber);
			body += commentedPage.getMarkdownListViewOfComments();
		}
		return body;
	}
	
	public SortedMap<Integer,List<Comment>> getCommentsByPage(){
		SortedMap<Integer,List<Comment>> result = new TreeMap<Integer,List<Comment>>();
		
		for (Integer pageNumber: commentedPages.keySet()){
			CommentedPdfPage page = commentedPages.get(pageNumber);
			result.put(pageNumber,page.getComments());
		}
		return result;
	}
	
	public List<Comment> getAllComments(){
		List<Comment> result = new ArrayList<Comment>();
		for (Integer pageNumber: commentedPages.keySet()){
			CommentedPdfPage page = commentedPages.get(pageNumber);
			result.addAll(page.getComments());
		}
		return result;
	}

	
	public SortedMap<Integer,List<Replace>> getReplacesByPage(){
		SortedMap<Integer,List<Replace>> result = new TreeMap<Integer,List<Replace>>();
		
		for (Integer pageNumber: commentedPages.keySet()){
			CommentedPdfPage page = commentedPages.get(pageNumber);
			result.put(pageNumber,page.getReplaces());
		}
		return result;
	}
	
	public String generatePatchedLaTeXSourceDocument(String sourceContent) {
	    return generatePatchedSourceDocument(sourceContent, SourceFormat.LaTeX);
	}
	
	public String generatePatchedMarkdownSourceDocument(String sourceContent) {
	    return generatePatchedSourceDocument(sourceContent, SourceFormat.Markdown);
	}
	
	public String generatePatchedSourceDocument(String sourceContent, SourceFormat sourceFormat) {

		StringBuffer result = new StringBuffer(sourceContent);

		SortedMap<Integer, List<Replace>> replacesByPage = getReplacesByPage();

		for (Integer pageNumber : replacesByPage.keySet()) {

			List<Replace> comments = replacesByPage.get(pageNumber);

			for (Replace comment : comments) {
				String toBeReplaced = comment.getReplacedSource();
				
				String toBeInserted = null;
				
				if (sourceFormat == SourceFormat.LaTeX)
				    toBeInserted = comment.getReplacingLaTeXSource();
				else
				    toBeInserted = comment.getReplacingMarkdownSource();

				if (toBeReplaced == null){
					System.err.println(format(
						"Couldn't find location to replace text [%s] with text [%s] on page number [%d] extracted from comment type [%s].",
						toBeReplaced, toBeInserted, pageNumber, comment.getClass().getSimpleName()));
					continue;
				}
				
				Integer startIndex = result.indexOf(toBeReplaced);
				Integer endIndex = startIndex + toBeReplaced.length();

				if (startIndex == -1)
					;
					// TODO log a warning.

				else
					result.replace(startIndex, endIndex, toBeInserted);
			}
		}
		
		return result.toString();
	}
		
}