package uk.ac.glasgow.pdfannot2latex.comments;

public interface Comment {
	
	public String getLaTeXText();
	
	public String getMarkdownText();

}
