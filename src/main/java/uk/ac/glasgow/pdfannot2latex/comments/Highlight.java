package uk.ac.glasgow.pdfannot2latex.comments;

public class Highlight extends Markup {

    private String highlightedText;
    private String comment;

    public Highlight(String preText, String highlightedText, String postText, String comment) {
	super(preText, postText);
	this.highlightedText = highlightedText;
	this.comment = comment;
    }

    public Highlight(String highlightedText, String comment) {
	this(null, highlightedText, null, comment);
    }

    @Override
    public String getLaTeXText() {
	return "``" + highlightedText + "''\n\n" + comment;
    }

    @Override
    public String getMarkdownText() {
	return "\"" + highlightedText +  "\"\n\n" + comment;
    }

}
