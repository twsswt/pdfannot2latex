package uk.ac.glasgow.pdfannot2latex.comments;

public class DocumentPatch extends Markup {

	private String textToRemove;
	private String textToInsert;

	public DocumentPatch(String preText, String textToRemove, String textToInsert, String postText) {
		super(preText, postText);
		this.textToRemove = textToRemove;
		this.textToInsert = textToInsert;
	}

	@Override
	public String getMarkdownText() {

		String preText = "..." + getPreText();
		String postText = getPostText() + "...";

		if (textToRemove.length() == 0)
			return preText + "_" + textToInsert + "_" + postText;
		else if (textToInsert.length() == 0)
			return preText + "-" + textToRemove + "-" + postText;
		else
			return preText + "-" + textToRemove + "-" + "_" + textToInsert + "_" + postText;
	}

	@Override
	public String getLaTeXText() {
		return "..." + getPreText() + "\\Replace{" + textToRemove + "}{" + textToInsert + "} " + getPostText() + "...";
	}

	public String toString() {
		return "[" + textToRemove + "] -> [" + textToInsert + "]";
	}

	public String getReplacedText() {
		String preText = getPreText();
		String postText = getPostText();
		if (textToRemove.length() == 0)
			return preText + " " + postText;
		else
			return preText + " " + textToRemove + " " + postText;
	}

	//public String getReplacingText() {

	//}

	public String getReplacingLaTeXSource() {
		String preText = getPreText();
		String postText = getPostText();

		if (textToRemove.length() == 0)
			return preText + "\\Replace{}{" + textToInsert + "} " + postText;
		else
			return preText + " \\Replace{" + textToRemove + "}{" + textToInsert + "} " + postText;
	}

	public String getReplacingMarkdownSource() {
		String preText = getPreText();
		String postText = getPostText();

		if (textToRemove.length() == 0)
			return preText + "_" + textToInsert + "_ " + postText;
		else if (textToInsert.length() == 0)
			return preText + "-" + textToRemove + "- " + postText;
		else
			return preText + "-" + textToRemove + "-" + "_" + textToInsert + "_ " + postText;
	}

}
