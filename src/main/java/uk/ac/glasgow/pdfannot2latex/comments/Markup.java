package uk.ac.glasgow.pdfannot2latex.comments;

public abstract class Markup implements Comment {
	
	private String preText;
	private String postText;

	public String getPreText() {
		return preText;
	}
	
	public String getPostText() {
		return postText;
	}
	
	public Markup(String preText, String postText){
		this.preText = preText == null ? "" : preText;
		this.postText = postText == null ? "" : postText;
	}
}
