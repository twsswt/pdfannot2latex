package uk.ac.glasgow.pdfannot2latex.comments;


public class Sticky implements Comment {

	private String comment;

	public Sticky(String comment) {
		this.comment = comment;
	}

	@Override
	public String getLaTeXText() {
		return comment;
	}

	@Override
	public String getMarkdownText() {
	    return comment;
	}

}
